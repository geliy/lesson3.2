var students = studentsAndPoints();

function studentsAndPoints() {
  var students = [];
  students.push(obj1 = {
    name: 'Алексей Петров',
    point: 0,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  students.push( obj2 = {
    name: 'Ирина Овчинникова',
    point: 60,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  students.push( obj3 = {
    name: 'Глеб Стукалов',
    point: 30,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  students.push( obj4 = {
    name: 'Антон Павлович',
    point: 30,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  students.push( obj5 = {
    name: 'Виктория Заровская',
    point: 30,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  students.push( obj6 = {
    name: 'Алексей Левенец',
    point: 70,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  students.push( obj7 = {
    name: 'Тимур Вамуш',
    point: 30,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  students.push( obj8 = {
    name: 'Евгений Прочан',
    point: 60,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  students.push( obj9 = {
    name: 'Александр Малов',
    point: 0,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  });
  return students;
}

students.push(
  obj10 = {
    name: 'Николай Фролов',
    point: 0,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  },
  obj11 = {
    name: 'Олег Боровой',
    point: 0,
    show: function () {
      console.log('Студент %s набрал %s баллов', this.name, this.point)
    }
  }
);

var addingPoints = students.map(function (student) {
  if (student.name == "Ирина Овчинникова" || student.name == "Александр Малов") {
    student.point += 30;
  }
  if (student.name == "Николай Фролов") {
    student.point += 10;
  }
});

var topStudents = students.filter(function (student) {
  if (student.point >= 30) {
    student.show();
    return true;
  } else {
    return false;
  }
});

var studentWorks = students.filter(function (student) {
  student.workAmount = student.point / 10;
  return true;
});

students.push(
  objStudents = {
    findByName: function () {
      var nameSearch = prompt("Кого ищем?");
      var checkName = students.filter(function (temp) {
        if (temp.name == nameSearch) {
          return true;
        } else {
          return false;
        }
      });
      console.log(checkName[0]);
    }
  }
);
